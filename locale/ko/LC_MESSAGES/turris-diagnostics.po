# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the turris-diagnostics package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: turris-diagnostics\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-04 19:49+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "run in background"
msgstr ""

msgid "print output to a file"
msgstr ""

msgid "print output to a directory module per file"
msgstr ""

msgid "modules:"
msgstr ""

msgid "Run Sentinel Certgen."
msgstr ""

msgid "List running processes."
msgstr ""

msgid ""
"Current and factory /etc/os-release file (/etc/turris-version for factory as "
"well)."
msgstr ""

msgid "Display output of dmesg."
msgstr ""

msgid "List detected hardware on USB and PCI."
msgstr ""

msgid "Test whether the Turris webs are available."
msgstr ""

msgid "List installed packages."
msgstr ""

msgid "Show info about Linux kernel."
msgstr ""

msgid "List all mount points and mount options and all block devices."
msgstr ""

msgid "Get the serial number from MOX OTP or atsha204."
msgstr ""

msgid "Dump configuration and try to run the updater."
msgstr ""

msgid ""
"Prints UCI network and wireless settings, current IP addresses, and routes."
msgstr ""

msgid "Show how long is the router running."
msgstr ""

msgid ""
"Examine running services and show whether the services are started after "
"boot."
msgstr ""

msgid "Read /var/log/messages."
msgstr ""

msgid "Info about the cron."
msgstr ""

msgid "Print current date and verify that NTP can synchronize time."
msgstr ""

msgid "Gather DNS related information."
msgstr ""

msgid "Lists U-Boot environment."
msgstr ""

msgid "Try to send firewall logs to the server."
msgstr ""

msgid "Print UCI firewall settings and the current content of iptables."
msgstr ""

msgid "Show mounted devices and examine files in /tmp/."
msgstr ""

msgid "Generate and send Turris notification."
msgstr ""
